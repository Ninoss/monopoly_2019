/*Η κλάςθ αυτι δθμιουργεί τα αντικείμενα/ςτιγμιότυπα των παικτών.
Θα πρζπει να περιλαμβάνει τισ μεταβλθτζσ για το id, το όνομα,
τθ θεση/το τετράγωνο ςτο οποίο βρίςκεται ο παίκτθσ και το ποςό των
χρημάτων του. Επίςθσ κα πρζπει να περιλαμβάνει ςυναρτιςεισ get για
τισ μεταβλθτζσ τθσ κλάςθσ. Τελοσ, κα πρζπει να περιλαμβάνει ςυναρτιςεισ
για τθν ενθμζρωςθ τθσ κζςθσ του παίκτθ, κακώσ επίςθσ και για τθν */

#include <iostream>
#include <sstream>

#include "player.h"

using namespace std;

int Player::getId(){
    return id;
}

int Player::getLocation(){
    return location;
}

int Player::getMoney(){
    return money;
}

string Player::getName(){
    return name;
}

Player::Player(int playerId){
    money = 1500;
    location = 0;
    id = playerId;
    if(playerId == 0)
        name = "Player 1";
    else if(playerId == 1)
        name = "Player 2";
}

void Player::setLocation(int spaceNum){
    location += spaceNum;
}

void Player::giveMoneyToPlayer(int amount){
    money += amount;
}

void Player::takeMoneyFromPlayer(int amount){
    money -= amount;
}
