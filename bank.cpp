#include "bank.h"

Bank::Bank(){
    money = 20580;
    freeParkMoney = 0;
}

int Bank::getMoney(){
    return money;
}

int Bank::getFreeParkMoney(){
    return freeParkMoney;
}

void Bank::takeMoneyFromBank(int amount){
    money -= amount;
}

void Bank::giveMoneyToBank(int amount){
    money += amount;
}

int Bank::takeFreeParkMoney(){
    int temp = freeParkMoney;
    freeParkMoney = 0;
    return temp;
}

void Bank::giveMoneyToFreePark(int amount){
    freeParkMoney += amount;
}
