#include <iostream>

#include "actions.h"

using namespace std;

int makeMove(Bank &bank, Player players[], int playerTurn, int diceRoll){
    players[playerTurn].setLocation(diceRoll);
    //dinei 200 an perasei apo afethria
    if(players[playerTurn].getLocation() > 39){
        players[playerTurn].setLocation(-40);
        players[playerTurn].giveMoneyToPlayer(200);
        bank.takeMoneyFromBank(200);
    }
    return players[playerTurn].getLocation();
}

void makeAction(Bank &bank, Player players[], Space spaces[], int playerTurn, int newSpace){
    //TYPES:
    //types(canbuy): Property, RailRoad, Utility
    //types(canNOTbuy): Tax, Entoli, Apofasi, Eksetastiki, Go, GoToEksetastiki, FreeParking
    //An einai canbuy kai anhkei ston allon paikth plhronei, an mporei na to agorasei to agorazei
    if(spaces[newSpace].getOwner() == players[1-playerTurn].getId() &&
        (spaces[newSpace].getType() == "Property" || spaces[newSpace].getType() == "Utility" || spaces[newSpace].getType() == "RailRoad")){

        players[playerTurn].takeMoneyFromPlayer(spaces[newSpace].getRent());
        players[1 - playerTurn].giveMoneyToPlayer(spaces[newSpace].getRent());
    }
    else if(spaces[newSpace].getOwner() == -1 && players[playerTurn].getMoney() >= 5*spaces[newSpace].getBuyingCost() &&
        (spaces[newSpace].getType() == "Property" || spaces[newSpace].getType() == "Utility" || spaces[newSpace].getType() == "RailRoad")){

        players[playerTurn].takeMoneyFromPlayer(spaces[newSpace].getBuyingCost());
        bank.giveMoneyToBank(spaces[newSpace].getBuyingCost());
    }
    //plhronei foro
    else if(spaces[newSpace].getType() == "Tax"){
        players[playerTurn].takeMoneyFromPlayer(spaces[newSpace].getTax());
        bank.giveMoneyToFreePark(spaces[newSpace].getTax());
    }
    //pernei kaseria apo poseidi
    else if(spaces[newSpace].getType() == "FreeParking"){
        players[playerTurn].giveMoneyToPlayer(bank.takeFreeParkMoney());
    }
    //an pesei sto GoToEksetastiki
    else if(spaces[newSpace].getType() == "GoToEksetastiki"){
        players[playerTurn].takeMoneyFromPlayer(200);
        bank.giveMoneyToFreePark(200);
        players[playerTurn].setLocation(-20);
    }

}
